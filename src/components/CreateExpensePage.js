import React from 'react';
import ExpenseForm from './ExpenseForm'
import {addExpense} from '../actions/expense'
import {connect} from 'react-redux'
const CreateExpensePage=(props)=>(

    <div>

   <h1> Welcome to Add Expense</h1>
    <ExpenseForm onSubmit={(expense)=>{
        props.dispatch(addExpense(expense))
        props.history.push('/')
    }} />
    
    </div>
    
)

export default connect()(CreateExpensePage);