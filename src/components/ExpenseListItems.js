import React from 'react';
import {removeExpense} from '../actions/expense'
import {connect} from 'react-redux'
import {Link} from'react-router-dom'
const ExpenseListItem=({id,description,amount,createdAt})=>(
  <div>
  <Link to={`/edit/${id}`} >{description}</Link>
  
  <p>{amount}-{createdAt}</p>
  
  </div>
)

export  default ExpenseListItem