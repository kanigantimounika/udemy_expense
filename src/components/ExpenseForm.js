import moment from 'moment';
import React from 'react';
import {SingleDatePicker} from 'react-dates'
import 'react-dates/lib/css/_datepicker.css';



export default class ExpenseForm extends React.Component{
    constructor(props)
    {
        super(props)
        this.state={ 
        description:props.expense?props.expense.description:'',
        note:props.expense?props.expense.note:'',
        amount: props.expense ? (props.expense.amount / 100).toString() : '',
        createdAt:props.expense?moment(props.expense.createdAt):moment(),
        calenderFocus:false,
        error:''

        }
    }
    

    DesciptionChange=(e)=>{
    console.log(e.target.value)
        const description=e.target.value;
        this.setState(()=>({description}))}
    Notechange=(e)=>{
        const note=e.target.value;
        this.setState(()=>({note}))}
    amountchange=(e)=>{
        const amount=e.target.value;
        if(!amount||amount.match(/^\d{1,}(\.\d{0,2})?$/))
        {
            this.setState(()=>({amount}))
        }
    }
    DateChange=(createdAt)=>{
        if (createdAt) {
            this.setState(() => ({ createdAt }));
          }
       

    }
    FocusChange=({focused})=>{
        this.setState(()=>({calenderFocus:focused}))
    }
    
    SubmitChange=(e)=>{
        e.preventDefault();
        if(!this.state.amount||!this.state.description)
        {
            this.setState(()=>({error:"Please enter valid amount and description"}))
        }
        else{

            this.setState(()=>({error:""}))
            this.props.onSubmit({
                description:this.state.description,
                amount: parseFloat(this.state.amount, 10) * 100,
                createdAt: this.state.createdAt.valueOf(),
                note:this.setState.note

            })
        }
    }
    render(){
        return(
        <div>
        {this.state.error && <p>{this.state.error}</p>}
        <form onSubmit={this.SubmitChange}>
        <input type="text" placeholder="Description" autoFocus value={this.state.description} onChange={this.DesciptionChange}></input>
        <input type="number" placeholder="amount" value={this.state.amount} onChange={this.amountchange}></input>
        <SingleDatePicker
        date={this.state.createdAt}
        onDateChange={this.DateChange}
        focused={this.state.calenderFocus}
        onFocusChange={this.FocusChange}
        numberOfMonths={1}
        isOutsideRange={()=>false}
        />
        <textarea  placeholder="Add the details " value={this.state.note} onChange={this.Notechange}></textarea>
        <button>AddExpense</button>
        </form>
        
        </div>
        )
    }
}
    

