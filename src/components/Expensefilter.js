import React from 'react';
import {connect} from 'react-redux'
import {setTextFilter,sortByAmount,sortByDate,setStartDate,setEndDate } from '../actions/filter'
import {DateRangePicker} from 'react-dates'


class ExpenseFilter extends React.Component{
     state={
        calenderFocused:null
    }
    DateChanges=({startDate,endDate})=>{
        console.log(endDate)
        this.props.dispatch(setStartDate(startDate));
        this.props.dispatch(setEndDate(endDate));

    }
    focusedChange=(calenderFocused)=>{
        this.setState(()=>({calenderFocused}))
    }
    render()
    {
        return(
            <div>
            <input type="text" value={this.props.filters.text} onChange={(e)=>{
                this.props.dispatch(setTextFilter(e.target.value))
            
        
            }}></input>
        
            <select value={this.props.filters.sortBy} onChange={
                (e)=>{
                    console.log(e.target.value)
                    if(e.target.value==='date')
                    {
                        console.log(e.target.value)
                        this.props.dispatch(sortByDate())
                    }
                    if(e.target.value==='amount'){
                        console.log(e.target.value)
                        this.props.dispatch(sortByAmount())
                    }
                }
            }>
            <option value="amount">Amount</option>
            <option value="date">Date</option>
            </select>
            <DateRangePicker
            startDate={this.props.filters.startDate}
            endDate={this.props.filters.endDate}
            onDatesChange={this.DateChanges}
            focusedInput={this.state.calenderFocused}
            onFocusChange={this.focusedChange}
            isOutsideRange={1}
            showClearDates={true}
            isOutsideRange={()=>false}
            
            >
            
            </DateRangePicker>
            </div>
        )
    }
}



const map=(state)=>{
    return {
        filters:state.filters
    }
}
export default connect(map)(ExpenseFilter)