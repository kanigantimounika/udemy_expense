import {Route,BrowserRouter,Switch,Link,NavLink} from 'react-router-dom';
import ReactDOM from 'react-dom';
import React from 'react';
import 'normalize.css/normalize.css'
import './styles/Mystyles.scss'
import AppRouter from './routers/AppRouter'
import configStore from './stores/store_config'
import {addExpense} from './actions/expense'
import {setTextFilter} from './actions/filter'
import getVisibleExpenses from './selectors/expense'
import {Provider} from 'react-redux'

const store=configStore();

store.dispatch(addExpense({description:'Rent',amount:109500}));
store.dispatch(addExpense({description:'water bill',amount:100,createdAt:100}));
store.dispatch(addExpense({description:'gas bill',amount:123,createdAt:1000}));
store.dispatch(setTextFilter('bill'));
const state=store.getState();
const visinble=getVisibleExpenses(state.expenses,state.filters)
setTimeout(()=>{
    store.dispatch(setTextFilter('rent'));
},3000)
console.log(visinble)
const jsx=(
    <Provider store={store}>
    <AppRouter/>
    </Provider>
)
ReactDOM.render(jsx, document.getElementById("root"))